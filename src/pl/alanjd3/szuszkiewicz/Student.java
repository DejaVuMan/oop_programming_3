package pl.alanjd3.szuszkiewicz;

import java.time.LocalDate;

public class Student extends Person implements Cloneable, Comparable<Person>{

    private double avgGrade;

    public Student(String name, LocalDate BD, double avgG){
        super(name, BD);
        this.avgGrade = avgG;
    }

    @Override
    public String toString(){
        return this.getClass().getSimpleName() + "( " + this.getName() + ", " + this.getBirthdate().toString()
                + ", " + this.avgGrade + " )";
    }

    @Override
    public int compareTo(Person comp){
        int last = super.compareTo((comp));
        if((comp instanceof Student) && (last == 0)){
            return -(int)Math.ceil(this.avgGrade - ((Student) comp).avgGrade);
        }
        return last;
    }
}
