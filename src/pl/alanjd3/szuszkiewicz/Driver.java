package pl.alanjd3.szuszkiewicz;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args){

        ArrayList<Person> people = new ArrayList<>(3);
        people.add(new Person("Bob", LocalDate.of(1999, 6, 18)));
        people.add(new Person("Jeremy", LocalDate.of(1984, 3, 12)));
        people.add(new Person("Alan", LocalDate.of(2001, 5, 6)));
        System.out.println(people.get(0));
        System.out.println(people.get(0).equals(people.get(1)));

        System.out.println(people);
        Collections.sort(people);
        System.out.println(people);
        //////////////////////////////////////////////

        ArrayList<Person> students = new ArrayList<>(3);
        students.add(new Student("Jacob", LocalDate.of(1999, 6, 18), 2.8));
        students.add(new Student("Jingleheimer", LocalDate.of(1984, 3, 12), 3.3));
        students.add(new Student("Schmidtt", LocalDate.of(2001, 5, 6), 4.01));

        System.out.println(students);
        Collections.sort(students);
        System.out.println(students);
        /////////////////////////////////////////////

        if(args.length != 0){
            ArrayList<String> a3 = new ArrayList<>();
            try{
                File tester = new File(args[0]);
                Scanner reader = new Scanner(tester);
                while(reader.hasNextLine()){
                    a3.add(reader.nextLine());
                }
                reader.close();
            }
            catch(FileNotFoundException a){
                System.out.println("This file does not exist or cannot be found.");
                a.printStackTrace();
            }
            System.out.println(a3);
            Collections.sort(a3);
            System.out.println(a3);
        }
    }
}
