package pl.alanjd3.szuszkiewicz;

import java.time.LocalDate;

public class Person implements Cloneable, Comparable<Person> {

    private String name;
    private LocalDate birthdate;

    public Person(String name, LocalDate BD){
        this.name = name;
        this.birthdate = BD;
    }

    public String getName(){
        return this.name;
    }

    public LocalDate getBirthdate(){
        return this.birthdate;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+"( "+this.name+", born on "+this.birthdate.toString()+" )";
    }

    @Override
    public boolean equals(Object obj) {
        Person osb = (Person) obj;
        return (osb.name.equals(this.name) && osb.birthdate.equals(this.birthdate));
    }

    @Override
    public int compareTo(Person comp) {
        int compare_nazwisko= this.name.compareTo(comp.name);
        if(compare_nazwisko==0){
            return this.birthdate.compareTo(comp.birthdate);
        }
        return compare_nazwisko;
    }
}