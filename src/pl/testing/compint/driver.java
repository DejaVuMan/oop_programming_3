package pl.testing.compint;

import java.util.*;

public class driver {
    public static void main(String[] args){
        ArrayList<comptest> al = new ArrayList<>();
        al.add(new comptest(101, "Burke", 23));
        al.add(new comptest(106, "Vi", 19));
        al.add(new comptest(103, "Doyle", 34));

        Collections.sort(al);
        for(comptest ct: al){
            System.out.println(ct.id + ", " + ct.name + ", " + ct.age + ".");
        }
    }
}
