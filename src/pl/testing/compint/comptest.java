package pl.testing.compint;

class comptest implements Comparable<comptest>{
    int id;
    String name;
    int age;
    comptest(int id, String name, int age){
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public int compareTo(comptest ct){
        // can be replaced with Integer.compare
        return Integer.compare(age, ct.age);
    }
}
