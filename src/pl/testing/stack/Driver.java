package pl.testing.stack;

import java.util.Stack;

import static pl.testing.stack.stacktest.*; // Import from file

public class Driver {

    public static void main(String[] args){
        Stack<Integer> stack = new Stack<>();

        stack_show(stack);
        stack_push(stack, 1);
        stack_show(stack);

        stack_push(stack, 4);
        stack_show(stack);

        stack_pop(stack);
        stack_show(stack);

        stack_peek(stack);
        stack_search(stack, 4);
        stack_search(stack, 1);
    }
}
