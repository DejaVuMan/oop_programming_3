package pl.testing.stack;
import java.util.*;

class stacktest {
    static void stack_push(Stack<Integer> stack, int a) {
            stack.push(a);
    }

    static void stack_pop(Stack<Integer> stack) {
        System.out.println("Popping: ");
            Integer y = stack.pop();
            System.out.println(y);
    }

    static void stack_peek(Stack<Integer> stack){
        Integer element = stack.peek();
        System.out.println("Top element: " + element);
    }

    static void stack_search(Stack<Integer> stack, int element){
        int pos = stack.search(element);

        if(pos == -1)
            System.out.println("Element not found...");
        else
            System.out.println("Element located at position " + pos);
    }

    static void stack_show(Stack<Integer> stack){
        System.out.println(stack);
    }
}
