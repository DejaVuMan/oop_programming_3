package pl.testing.lambinterf;

import java.util.Comparator;

public class lambexample {
    public static void main(String[] args){
        Comparator<String> stringComparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2); // standard way of comparing strings
            }
        }; // Anonymous implementation

        int compare = stringComparator.compare("hello", "world");
        System.out.println(compare);
        // returns -15 because first string is before second when compared alphabetically.

        // Below is a lambda implementation
        Comparator<String> stringComparatorLambda =
                (String o1, String o2) -> { return o1.compareTo(o2); };
                //     Parameters      ->          Function
        // this can also be replaced with: String::compareTo;
        // because Java 8+ has some level of type inference, we can replace "String o1" with just "o1"
        // with single line lambda's, we don't even need the curly brackets or "return" word.

        int compareL = stringComparatorLambda.compare("hello", "world");
        System.out.println(compareL);
    }
}
