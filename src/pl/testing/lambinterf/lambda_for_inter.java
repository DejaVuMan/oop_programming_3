package pl.testing.lambinterf;

import java.util.Comparator;

public class lambda_for_inter {
    public static void main(String[] args){
        InterfaceBasic interfaceBasic = (s1, s2) -> {
            System.out.println(s1 + " + " + s2);
            return s1 + " + " + s2;
        };
        //                              Empty parenthesis for no parameters
        //                              One parameter doesnt need parenthesis
        //                              More than 1 requires parenthesis again
        // If function returns something, you must create a method body
        String returnVal = interfaceBasic.apply("Hello Function Body", "How are you today?");

        System.out.println(returnVal); // Prints System.out in body first then return
    }
}

