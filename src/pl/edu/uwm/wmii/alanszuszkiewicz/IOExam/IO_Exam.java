package pl.edu.uwm.wmii.alanszuszkiewicz.IOExam;

import java.util.ArrayList;
import java.util.Random;

public class IO_Exam {

    public static void main(String[] args) {
        primer(5);

        ArrayList<Integer> arr1 = new ArrayList<>(8);
        for(int i=0; i<8; i++)
            arr1.add(i, i+1);

        ArrayList<Integer> arr2 = new ArrayList<>(4);
        for(int i=0; i<4; i++)
            arr2.add(i, i+8);

        System.out.println(arr1);
        System.out.println(arr2);
        System.out.println(mergeSort(arr1, arr2));
    }
    public static void primer(int n) {
        int[] holder = new int[n];

        for (int i = 0; i < n; i++) {
            Random random = new Random();
            int int_random = random.nextInt(25 - (-25)) + (-25);
            holder[i] = int_random;
        }

        for (int j = 0; j < n; j++) {
        if(holder[j] % 2 > 0)
            System.out.println(holder[j]);
        }
    }
    public static ArrayList<Integer> mergeSort(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> arrMerged = new ArrayList<>();
        int ca = 0;
        int cb = 0;
        while(ca < a.size() || cb < b.size()){
            if(ca < a.size())
                arrMerged.add(a.get(ca++));
            if(cb < b.size())
                arrMerged.add(b.get(cb++));
        }
        for(int j = 0; j < arrMerged.size(); j++){
            for(int k = arrMerged.size()-1; k > j; k--){
                if(arrMerged.get(j) > arrMerged.get(k)){
                    int temp = arrMerged.get(j);
                    arrMerged.set(j, arrMerged.get(k));
                    arrMerged.set(k, temp);
                }
            }
        }

        for(int j = 0; j < arrMerged.size()-1; j++){
            if(arrMerged.get(j).equals(arrMerged.get(j + 1)))
                arrMerged.remove(j);
        }

        return arrMerged;
    }
}
