package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium00;

public class Zadanie7 {

    public static void main(String[] args) {

             System.out.println("     ___       __          ___      .__   __.         _______.");
             System.out.println("    /   \\     |  |        /   \\     |  \\ |  |        /       |");
             System.out.println("   /  ^  \\    |  |       /  ^  \\    |   \\|  |       |   (----`");
             System.out.println("  /  /_\\  \\   |  |      /  /_\\  \\   |  . `  |        \\   \\");
             System.out.println(" /  _____  \\  |  `----./  _____  \\  |  |\\   |    .----)   |");
             System.out.println("/__/     \\__\\ |_______/__/     \\__\\ |__| \\__|    |_______/");
    }

    /*
    You can also use a text block!
    String name = """
                    ***** *    ***** *   *
                    *   * *    *   * * * *
                    ***** *    ***** *  **
                    *   * **** *   * *   *
                  """
               System.out.println(name);

               Alternatively, you can also use String Builder

     String name = "....\n" +
                   "....\n" ;


     */

}
