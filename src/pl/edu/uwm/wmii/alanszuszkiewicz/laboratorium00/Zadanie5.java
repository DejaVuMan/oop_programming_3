package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium00;

public class Zadanie5 {
    public static void main(String[] args) {
        box("Java"); /* Assigns String Value to surround with box */
    }

    public static void box(String contents){
        int n = contents.length(); /*Looks at length of string */
        System.out.print("*"); /* First Asterisk */
        for (int i = 2; i < n + 2; i++){
            System.out.print("-"); /*Prints out dashes depending on length of phrase */
        }
        System.out.print("*");/* Last Asterisk */
        System.out.println();/* New Line */
        System.out.println("|" + contents + "|"); /* Vertical Lines on side of word */
        System.out.print("*");/* Bottom First Asterisk*/
        for (int i = 2; i < n + 2; i++){ /* i is set to begin at 2, and grows incrementally until less than n + 2*/
            System.out.print("-"); /*Prints out Dashes on bottom*/
        }
        System.out.print("*"); /* Last Asterisk*/
        System.out.println();/* New Line*/
    }
}



/*     *----*
       |Epic|
       *----*
 */