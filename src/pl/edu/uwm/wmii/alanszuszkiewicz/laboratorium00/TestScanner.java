package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium00;

import java.util.Scanner;

public class TestScanner {

    public static void main(String[] args){

        Scanner in = new Scanner(System.in);

        System.out.print("Provide a real number: ");
        double x = in.nextDouble();

        System.out.print("Provide a whole number: ");
        int n = in.nextInt();

        System.out.println(x + "^" + n + " = " + Math.pow(x, n));
        System.out.printf("%.3f^%d = %.3f%n", x, n, Math.pow(x, n));


    }
}
