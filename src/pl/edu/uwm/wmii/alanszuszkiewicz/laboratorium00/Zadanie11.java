package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium00;

public class Zadanie11 {

    public static void main(String[] args){
        System.out.println("This poem is called \"The Road not Taken,\" written by Robert Frost.");
        System.out.println("Two roads diverged in a yellow wood,");
        System.out.println("        And sorry I could not travel both");
        System.out.println("And be one traveler, long I stood");
        System.out.println("And looked down one as far as I could");
        System.out.println("To where it bent in the undergrowth;");
        System.out.println();
        System.out.println("Then took the other, as just as fair,");
        System.out.println("        And having perhaps the better claim,");
        System.out.println("        Because it was grassy and wanted wear;");
        System.out.println("Though as for that the passing there");
        System.out.println("Had worn them really about the same,");
        System.out.println();
        System.out.println("        And both that morning equally lay");
        System.out.println("In leaves no step had trodden black.");
        System.out.println("        Oh, I kept the first for another day!");
        System.out.println("        Yet knowing how way leads on to way,");
        System.out.println("        I doubted if I should ever come back.");
        System.out.println();
        System.out.println("        I shall be telling this with a sigh");
        System.out.println("Somewhere ages and ages hence:");
        System.out.println("Two roads diverged in a wood, and I—");
        System.out.println("I took the one less traveled by,");
        System.out.println("        And that has made all the difference.");

    }

}
