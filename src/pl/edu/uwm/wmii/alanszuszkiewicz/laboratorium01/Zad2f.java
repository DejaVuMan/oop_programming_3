package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium01;

public class Zad2f {
    // odd array position value and are even values.
    public static void main(String[] args){

        int[] sequence = {1,2,4,6,7,8,10,20,30,40};

        int count = 0;
        for(int i =1; i< sequence.length; i++){

            if(i % 2 != 0 & sequence[i]%2 == 0) {
                System.out.println("The number " + sequence[i] + " fulfills all criteria (Position " + i +")");
                count++;
            }

        }
        System.out.println(count);
    }

}
