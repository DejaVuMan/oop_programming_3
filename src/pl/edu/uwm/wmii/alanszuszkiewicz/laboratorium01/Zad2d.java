package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium01;

public class Zad2d {

    public static void main (String[] args){

        int[] sequence = {2,3,4,5,6,7,8,9,10};

        int count = 0;
        for( double v : sequence){
            if(v < ((v - 1 + v + 1)/2) & v > 1 & v < sequence.length) {
                count = count + 1;
                System.out.println("The number " + v + " fulfills all criteria.");
            }
        }
    System.out.println(count);
    }
}
