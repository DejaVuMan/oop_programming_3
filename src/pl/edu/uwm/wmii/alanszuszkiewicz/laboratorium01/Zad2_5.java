package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium01;

public class Zad2_5 {

    public static void main(String[] args){
        int[] sequence = {1,5,-20,400,-10,8,9,10};
        int pairs = 0;
        for(int i = 0; i< sequence.length-1; i++) {

            if (sequence[i] > 0 & sequence[i + 1] > 0) {
                pairs++;
                System.out.println("The positive pairs (" + sequence[i] + "," + sequence[i + 1] + ") exist.");
            }

        }
        System.out.println("Total number of pairs: " +pairs);
    }
}
