package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium01;

import java.util.Scanner;

public class Placeholder {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Provide a real number: ");
        double x = in.nextDouble();

        System.out.println(x + " * 2 = " + (x*2));

    }
}
