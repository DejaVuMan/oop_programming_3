package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium01;

public class Zad2_2 {
        public static void main(String[] args) {
            // Calculate the double of the sum of those numbers in array which are positive
            // So 2(a1+a2+a3+a4)
            int[] sequence = {1, 5, 12, -4, 6, -1, -3, 10, 9, 8};
            int total = 0;

            for (int j : sequence) {
                if (j > 0) {
                    total = total + j;
                }
            }
            System.out.println("Total before doubling amount of positive numbers: " + total);
            System.out.println("Total after doubling amount of positive numbers: " + (total*2));
        }
}
