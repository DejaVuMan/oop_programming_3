package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium01;

public class Zad2b {

    public static void main(String[] args){

        int[] sequence = {1,3,6,9,12,13,7,8,2,10};
        int count = 0;
        for (int j : sequence) {
            int div3 = Math.abs(j) % 3;
            int div5 = Math.abs(j) % 5;

            if (div3 == 0 & div5 > 0) {
                count += 1;
                System.out.println("Number " + j + " is divisible by 3 and not divisible by 5.");
            }
        }
        System.out.println("There are " + count + " such numbers.");
    }
}
