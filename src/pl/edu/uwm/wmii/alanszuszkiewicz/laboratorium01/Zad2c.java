package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium01;

public class Zad2c {

    public static void main (String[] args){

        //Squares of even numbers are even (and in fact divisible by 4), since (2n)^2 = 4n^2
        // square

        double[] sequence = {1,2,4,6,9,16,36,64,100};
        int count = 0;
        for (double v : sequence) {
            if (v % 4 == 0 & Math.pow(v, 2) % 2 == 0) {
                count += 1;
                System.out.println("The number " + v + " is a square of an even number");
            }
        }

        System.out.println(count);
    }
}
