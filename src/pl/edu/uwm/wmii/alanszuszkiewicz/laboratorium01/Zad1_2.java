package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium01;

public class Zad1_2 {

    public static void main(String[] args) {

        //Initialize array
        int [] arr = new int [] {1, 2, 3, 4, 5};
        //n determines the number of times the array should be rotated.
        int n = 4;

        //Displays the original array
        System.out.println("Original array: ");
        for (int k : arr) {
            System.out.print(k + " ");
        }

        //Rotate the given array by n times towards the right
        for(int i = 0; i < n; i++){
            int j, last;
            //Stores the last element of the array
            last = arr[arr.length-1];

            for(j = arr.length-1; j > 0; j--){
                //Shift the elements of the array by one
                arr[j] = arr[j-1];
            }
            //Last element of array will be added to the start of array.
            arr[0] = last;
        }

        System.out.println();

        //Displays resulting array after rotation
        System.out.println("Array after right rotation: ");
        for (int j : arr) {
            System.out.print(j + " ");
        }
    }
}


