package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium01;

public class Zad2a {

    public static void main(String[] args){

        int[] sequence = {1,2,3,4,5,6,7,8,9,10};
        int count = 0;
        for(int i =0; i< sequence.length;i++){
            if (Math.abs(sequence[i]) % 2 != 0) {
                count += 1;
                System.out.println("Number " + sequence[i] + " is not divisible");
            }
        }
        System.out.println("There are " + count + " odd numbers.");
    }

}
