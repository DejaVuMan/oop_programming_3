package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium01;

public class Zad2_3 {
    public static void main(String[] args) {
        // Calculate amount of positives, negatives, and zeros.
        int[] sequence = {1, 0, 12, -4, 6, -1, -3, 10, 9, 8};
        int pos = 0;
        int neg = 0;
        int zero = 0;

        for (int j : sequence) {
            if (j == 0)
                zero++;
            if (j > 0)
                pos++;
            if (j < 0)
                neg++;
        }
        System.out.println("There were " + pos + " positive numbers.");
        System.out.println("There were " + neg + " negative numbers.");
        System.out.println("There were " + zero + " zeroes.");
    }
}
