package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium01;
import java.lang.Math;
// Basically, Make an array N long
public class Zad1 {

    public static int factorial(int test){
        int i,fact=1;
        for(i=1; i<= test; i++){
            fact=fact*i;
        }
        return fact;
    }

    public static void main(String[] args) {



// Adding together numbers in array
        int[] sequence = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        double[] absol = {-1,2,4,8,-5,12};
        int resultadd = 0;
        for (int j : sequence) {
            resultadd = resultadd + j;
        }
        //Multiplying numbers together in array
        int resultmult = 1;
        for (int j : sequence) {
            resultmult = resultmult * j;
        }
        //Absolute numbers added in array
        double absolres = 0;
        for (double j : absol) {
            if (j > 0)
                absolres = absolres + j;
            else
                absolres = absolres + (j * -1);
        }
        //Absolute sqrt numbers added in array
        double sqrtres = 0;
        for (double j : absol) {
            if (j > 0)
                sqrtres = sqrtres + Math.sqrt(j);
            else
                sqrtres = sqrtres + Math.sqrt((j * -1));
        }
        //Absolute value multiplication in array
        double absolmult = 1;
        for (double j : absol) {
            if (j > 0)
                absolmult = absolmult + j;
            else
                absolmult = absolmult + (j * -1);
        }
        //Squared valued added in array
        int squareres = 0;
        for (int j : sequence) {
            squareres += Math.pow(j, 2);
        }
        //Subtract then add subtract then add
        // basically A S A S A S...
        int subadd = 0;
        for(int i =0; i< sequence.length; i++){

            if(i == 0 | i % 2 ==0)
                subadd = subadd + sequence[i]; // Add if first in array or even index
            else{
                subadd = subadd - sequence[i]; // Subtract if odd index
            }
        }
        // negative a1/1! then add a2/2!...
        double[] doubleseq = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; // REMINDER! If Int is divided by Int, an Int is returned.
        double factflip = 0;                                 // And it is also rounded. You must at least have ONE
        for(int i =0; i< doubleseq.length; i++){            // Double.
            if(i == 0 | i % 2 ==0)
                factflip = factflip - (doubleseq[i]/factorial((i+1)));
            else{
                factflip = factflip + (doubleseq[i]/factorial((i+1)));
            }
            //System.out.println(factflip);
        }

        System.out.println("Sum of additive Series: " + resultadd);
        System.out.println("Product of multiplicative Series: " + resultmult);
        System.out.println("Sum of absolute value Series: " + absolres);
        System.out.println("Sum of square root Series: " + sqrtres);
        System.out.println("Product of absolute value Series: " + absolmult);
        System.out.println("Product of squared value Series: " + squareres);
        System.out.println("Sum and product of Series: " + resultadd + " and " +resultmult);
        System.out.println("Sum of Add/Subtract Series: " + subadd);
        System.out.println("Sum of Add/Subtract divided by factorial Series: " + factflip);
        //System.out.println()
    }
}

