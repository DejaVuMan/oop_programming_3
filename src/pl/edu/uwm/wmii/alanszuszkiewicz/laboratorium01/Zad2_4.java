package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium01;

public class Zad2_4 {

    // Find smallest and largest number in array

    public static void main(String[] args){

        int[] sequence = {1,5,20,400,-10,8,9,10};
        int smallest = 999;
        int largest = 0;

        for(int i = sequence.length - 1; i >= 0; i--){
            if(sequence[i] < smallest)
                smallest = sequence[i];
            if(sequence[i] > largest)
                largest = sequence[i];
        }
        System.out.println("The smallest number in the array is: " + smallest);
        System.out.println("The largest number in the array is: " + largest);
    }

}
