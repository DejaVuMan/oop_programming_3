package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium01;





public class Zad2e {

    public static int factorial(int test){
        int i,fact=1;
        for(i=1; i<= test; i++){
            fact=fact*i;
        }
        return fact;
    }

    public static void main(String[] args){

        int[] sequence = {1,2,3,4,5,10,20,69,420};

        int count = 0;
        for(int i=0; i<sequence.length; i++){
            if(sequence[i] > (2^i) & (sequence[i] < factorial(i)))
                count = count +1;
                System.out.println("The number " + sequence[i] + " satisfies the requirements.");

        }
        System.out.println(count);
    }

}
