package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium02;

import java.util.Arrays;
import java.util.Random;

public class Zad2 {
    public static void main(String[] args) { //change int to void
        int[] array = new int[22];
        generate(array, 21, -20, 20);
        System.out.println(OddVals(array));
        System.out.println(EvenVals(array));
        System.out.println(Positive(array));
        System.out.println(Negative(array));
        System.out.println(Zeros(array));
        System.out.println(Maximum(array));
        System.out.println(SumPos(array));
        System.out.println(NegPos(array));
        System.out.println(PositiveLen(array,21));
        Signum(array);
        //Rotate(array);
    }
        public static int getRandomNum(int min, int max){
            Random random = new Random();
            return random.nextInt(max-min)+min;
        }

        public static void generate(int[] tab, int n, int min, int max){
            System.out.print("[");
            for (int i=0; i<=n; i++){
                tab[i] = getRandomNum(min, max);
                System.out.print(tab[i] + " ");
            }
            System.out.print("]");
            System.out.println();
        }
        public static int OddVals(int[] tab){
            int OddCount = 0;
            for(int elem: tab){
                if(!(elem % 2 ==0))
                    OddCount++;
                }
            return OddCount;
            }

        public static int EvenVals(int[] tab) {
            int EvenCount = 0;
            for (int elem : tab) {
                if (elem % 2 == 0)
                    EvenCount++;
            }
            return EvenCount;
        }

    public static int Positive(int[] tab) {
        int Pos = 0;
        for (int elem : tab) {
            if (elem >= 0)
                Pos++;
        }
        return Pos;
    }

    public static int Negative(int[] tab) {
        int Neg = 0;
        for (int elem : tab) {
            if (elem < 0)
                Neg++;
        }
        return Neg;
    }

    public static int Zeros(int[] tab) {
        int Zer = 0;
        for (int elem : tab) {
            if (elem == 0)
                Zer++;
        }
        return Zer;
    }

    public static int Maximum(int[] tab) {
        int big = -999;
        for (int elem : tab) {
            if (elem > big)
                big = elem;
        }
        return big;
    }

    public static int SumPos(int[] tab) {
        int SumPos = 0;
        for (int elem : tab) {
            if (elem >= 0)
                SumPos = SumPos + elem;
        }
        return SumPos;
    }

    public static int NegPos(int[] tab) {
        int NegPos = 0;
        for (int elem : tab) {
            if (elem < 0)
                NegPos = NegPos + elem;
        }
        return NegPos;
    }

    public static int PositiveLen(int[] tab, int n) {
        int maxLen = 0, currLen = 0;

        for (int k = 0; k < n; k++){
            if(tab[k] > 0){
                currLen++;
            }
            else{
                if(currLen > maxLen){
                    maxLen = currLen;
                }
                currLen = 0;
            }
        }
        if(maxLen > 0){
            return maxLen;
        }
        else
            return 0;
    }
    public static void Signum(int[] tab) {
        System.out.print("[");
        for (int i = 0; i < (tab.length); i++) {
            if (tab[i] < 0)
                System.out.print("-1 ");
            else if (tab[i] == 0)
                System.out.print("0 ");
            else if (tab[i] > 0)
                System.out.print("1 ");
        }
        System.out.print("]");
        System.out.println();
    }
    /* Left and Right are index barriers for from where we want to rotate
    public static void Rotate(int[] tab){
        int[] master = tab;
        int[] reverse = tab;
        int j = 0;
        for(int i=(tab.length -1); i>=0; i--){
            reverse[j] = tab[i];
            j++;
        }
        System.out.println(Arrays.toString(reverse));
    } */
}

