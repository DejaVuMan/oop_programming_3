package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium02;

import java.util.Random;

public class Zad1 {
    public static void main(String[] args){
        int Amount = 20;
        int max = 99;
        int min = -99;
        int[] array = new int[Amount];


        System.out.print("[ ");
        for (int i=0; i<Amount; i++){
            Random random = new Random();
            int int_random = random.nextInt(max-min)+min;
            array[i] = int_random;
            System.out.print(array[i] + " ");
        }
        System.out.print("]");
        System.out.println();


        int OddCount = 0;
        int EvenCount = 0;
        for(int elem: array){
            if(!(elem % 2 ==0))
                OddCount++;
            else
                EvenCount++;
        }
        System.out.println("Amount of Even values: " + EvenCount + " | Amount of Odd Values: " + OddCount);


        int Neg = 0;
        int Pos = 0;
        int Zeros = 0;
        for (int elem : array) {
            if (elem < 0)
                Neg++;
            else if (elem > 0)
                Pos++;
            else
                Zeros++;
        }
        System.out.println("Amount of Positive, Negative, and Zero values: " + Pos + ", " + Neg + ", " + Zeros + ".");


        int big = -999;
        int occur = 0;
        for (int elem : array) {
            if (elem > big)
                big = elem;
        }
        for(int elem : array){
            if(elem == big)
                occur++;
        }
        System.out.println("The largest element that occurs " + occur + " time(s) is: " + big);

        int SumPos = 0;
        int NegPos = 0;
        for (int elem : array) {
            if (elem >= 0)
                SumPos = SumPos + elem;
            else
                NegPos = NegPos + elem;
        }
        System.out.println("The sum of positive values is: " + SumPos + " | The sum of negative values is: " + NegPos);


        int maxLen = 0, currLen = 0;
        for (int i = 0; i < Amount; i++){
            if(array[i] > 0){
                currLen++;
            }
            else{
                if(currLen > maxLen){
                    maxLen = currLen;
                }
                currLen = 0;
            }
        }
        if(maxLen > 0)
            System.out.println("The longest positive chain of numbers is " + maxLen + " number(s) long.");
            else
                System.out.println("There are no positive numbers forming a chain.");


        System.out.print("[ ");
        for (int i = 0; i < Amount; i++) {
            if (array[i] < 0)
                System.out.print("-1 ");
            else if (array[i] == 0)
                System.out.print("0 ");
            else if (array[i] > 0)
                System.out.print("1 ");
        }
        System.out.print("]");
        System.out.println();
    }
}
