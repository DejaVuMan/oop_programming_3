package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium05;

public class BankAccount {

    private static double percentage;
    private double balance;

    public static void main(String[] args){
        BankAccount test = new BankAccount();
        setPercentage(0.10);
        test.Account(2000);
        System.out.println(test.getBalance());
        test.findpercentage();
        System.out.println(test.getBalance());
    }

    public void Account(double balance){
        this.balance = balance;
    }
    public void findpercentage(){
        this.balance = this.balance + (this.balance * percentage) / 12;
    }
    public static void setPercentage(double level){
        percentage = level;
    }
    public double getBalance() {
        return this.balance;
    }
}