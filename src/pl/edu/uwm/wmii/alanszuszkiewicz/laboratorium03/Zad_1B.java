package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium03;

public class Zad_1B {

    public static void main(String[] args){
        System.out.println(customcounter("epiccc", "e"));
    }

    public static String customcounter(String str, String subStr){
        int counter = 0;

        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == subStr.charAt(0))
                counter++;
        }
        return "The string " + str + " contains " + counter + " occurrence(s) of the letter " + subStr;
    }
}

