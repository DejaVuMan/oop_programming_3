package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium03;

public class Zad_1A {

    public static void main(String[] args){
        System.out.println(counter("epiccc"));
    }

    public static String counter(String str){
        int counter = 0;

        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == 'c')
                counter++;
        }
        return "The string " + str + " contains " + counter + " occurrence(s) of the letter C.";
    }
}
