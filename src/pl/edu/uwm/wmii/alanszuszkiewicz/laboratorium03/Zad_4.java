package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium03;

import java.math.BigInteger;

public class Zad_4 {
    public static void main(String[] args) {
        System.out.println(poppy_seeds(3)); // 3x3 table
        // 1 2 4
        // 8 16 32
        //64 128 256
    }
    static BigInteger poppy_seeds(int n){
        BigInteger[][] arr = new BigInteger[n][n];
        BigInteger count = BigInteger.valueOf(1);
        int i = 0;
        while(i < n){
            for (int j = 0; j < n; j++){
                arr[i][j] = count;
                count = count.multiply(BigInteger.valueOf(2));
            }
            i++;
        }
        BigInteger total = BigInteger.valueOf(0);
        i = 0;
        while(i < n){
            for (int j = 0; j < n; j++)
            {
                total = total.add(arr[i][j]);
            }
            i++;
        }
        return total;
    }
}
