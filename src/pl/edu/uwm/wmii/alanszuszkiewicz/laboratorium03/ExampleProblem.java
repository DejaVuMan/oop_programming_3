package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium03;

public class ExampleProblem {

    public static void main(String[] args) {

        System.out.println(middle("gamer"));

    }
        public static String middle(String str){

            String m = "";
            if(str.length() <= 1){
                return "The string does not have a middle";
            }
            if (str.length() > 1) {
                int ml = (int)str.length()/2;
                if(str.length()%2 == 0)
                    m = str.substring(ml-1, ml+1);
                else
                    m = str.substring(ml, ml+1);
            }
            return m;
        }
    }
