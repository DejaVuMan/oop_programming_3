package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium03;

public class Zad_1D {

    public static void main(String[] args){
        System.out.println(repeat("ho", 3));
    }

    public static String repeat(String str, int n){
        //String s = "";
        //for (int i =0; i<n; i++){
        //    s.concat(str);
        //}
        //return s;
        // using concat normally does not actually modify the original value.
        // as long as you have Java 11 and above, this is all that is necessary
        return str.repeat(n);
        // of course, this can potentially cause compatibility issues.
        // if you're stuck on Java 8, this would also work:
        // return String.join("", Collections.nCopies(n, str));
        // Don't forget to import java.util.Collections !!!
    }
}
