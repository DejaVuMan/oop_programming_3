package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium03;

public class ExampleProblem2 {

    public static void main(String[] args) {

        System.out.println(change("gaMer"));
    }

    public static StringBuffer change(String str) {

        char[] chararray = new char[str.length()];
        StringBuffer s = new StringBuffer(str.length());

        for (int i = 0; i < str.length(); i++) {
            chararray[i] = str.charAt(i);
        }

        char[] NChararray = new char[str.length()];

        for (int k = 0; k < str.length(); k++) {
            if (Character.isUpperCase(chararray[k])) {
                NChararray[k] = Character.toLowerCase(chararray[k]);
            } else
                NChararray[k] = Character.toUpperCase(chararray[k]);
        }
        for (int j = 0; j < NChararray.length; j++) {
            s.append(NChararray[j]);
        }
        return s;
    }
}
