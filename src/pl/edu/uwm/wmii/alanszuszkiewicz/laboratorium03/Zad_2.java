package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium03;

import java.io.File;  // Import the File class
import java.io.FileNotFoundException;  // Import this class to handle errors
import java.util.Scanner; // Import the Scanner class to read text files

public class Zad_2 {

    public static void main(String[] args){
        String a = "C:/GitRepo/oop_programming_3/src/pl/edu/uwm/wmii/alanszuszkiewicz/laboratorium03/fox.txt";
        // If running on a Windows Machine
        System.out.println(result("t",a));
    }
    public static int result(String str, String lf){
        int counter = 0;
        try{ File obj = new File(lf);
             Scanner reader = new Scanner(obj);

             while(reader.hasNextLine()){
                 String data = reader.nextLine();

                 for(int i =0; i < data.length(); i++){

                     if (data.charAt(i) == str.charAt(0))
                         counter ++;
                 }
             }
            reader.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("An error occurred. Maybe the file path is incorrect?");
            e.printStackTrace();
            return 0;
        }
        return counter;
    }
}
