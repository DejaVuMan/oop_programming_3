package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium03;

import java.util.Arrays;

public class Zad_1E {

    public static void main(String[] args){
        System.out.println(Arrays.toString(where("craaaaaazy taxi!", "a")));
    }

    public static int[] where(String str, String subStr) {
        int pos = 0;
        int len = 0;

        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == subStr.charAt(0))
              len++;
        }

        int[] locator = new int[len];

        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == subStr.charAt(0)) {
                locator[pos] = i;
                pos++;
            }
        }
        return locator;
    }
}
