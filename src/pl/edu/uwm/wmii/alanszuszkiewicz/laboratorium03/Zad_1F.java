package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium03;

public class Zad_1F {

    public static void main(String[] args) {

        System.out.println(change("gaMer"));
    }

    public static StringBuffer change(String str){

        char[] chararray = str.toCharArray();
        StringBuffer s = new StringBuffer(str.length());

        char[] NChararray = new char[str.length()];

        for (int k = 0; k < str.length(); k++) {
            if (Character.isUpperCase(chararray[k])) {
                NChararray[k] = Character.toLowerCase(chararray[k]);
            } else
                NChararray[k] = Character.toUpperCase(chararray[k]);
        }
        for (char c : NChararray) {
            s.append(c);
        }
        return s;
    }
}
