package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium03;

public class Zad_1G {

    public static void main(String[] args) {

        System.out.println(nice("1234567890"));
    }

    public static StringBuilder nice(String str) {
        // Instead of using StringBuffer, it would be much easier to use StringBuilder.
        // Alot more memory efficient I believe, as well as being smaller in size & complexity.
        StringBuilder s = new StringBuilder(str);
        int id = (str.length() - 3);

        while (id > 0)
        {
            s.insert(id, ",");
            id = id - 3;
        }
        return s;
    }
}
