package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium03;

public class Zad_1H {

    public static void main(String[] args) {

        System.out.println(nice("1234567890", "!", 0));
    }

    public static StringBuilder nice(String str, String select, int sep) {
        StringBuilder s = new StringBuilder(str);
        int id = (str.length() - sep);

        if (sep < 1) // Saves time and memory by not having to use while loop in certain scenarios.
            return s;

        while (id > 0)
        {
            s.insert(id, select);
            id = id - sep;
        }
        return s;
    }
}
