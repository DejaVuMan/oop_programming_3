package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium03;

public class Zad_1C {

    public static void main(String[] args){
        System.out.println(middle("test"));
    }

    public static String middle(String str){

        String s_mid = "";
        if(str.length() <= 1)
            return "This string is too short to have a character in the middle!";

        int ml = (int)str.length()/2;
        if(str.length()%2 == 0) {
            s_mid = str.substring(ml - 1, ml + 1);

            System.out.println("Your string does not have a 'true' middle, as it is of an even length.");
            return "The approximated middle of your string is: " + s_mid;
        }
        else
            s_mid = str.substring(ml, ml+1);
        return "The middle of your string is: " + s_mid;
    }
}
