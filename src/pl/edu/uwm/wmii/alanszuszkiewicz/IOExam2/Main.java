package pl.edu.uwm.wmii.alanszuszkiewicz.IOExam2;

import java.util.ArrayList;
import java.util.Collections;

import static pl.edu.uwm.wmii.alanszuszkiewicz.IOExam2.Kandydat.Qualified;

public class Main {
    public static void main(String[] args) {
        ArrayList<Kandydat> potential = new ArrayList<>();
        potential.add(new Kandydat("Jan Kowalski", 28, "Doctorate", 5));
        potential.add(new Kandydat("Andrzej Sulikowski", 28, "Masters", 5));
        potential.add(new Kandydat("John Doe", 21, "Masters", 5));
        potential.add(new Kandydat("Janette Doe", 39, "Masters", 5));
        potential.add(new Kandydat("Elon Musk", 28, "Bachelors", 5));
        potential.add(new Kandydat("Nega Musk", 28, "Bachelors", 20));

        System.out.print("\nBefore modifications:\n");
        for (Kandydat e : potential) {
            System.out.printf("%s is %d years old, has a %s, with %d years of experience\n",
                    e.getNazwa(), e.getWiek(), e.getWyksztalcenie(), e.getDoswiadczenie());
        }

        Collections.sort(potential);

        System.out.print("\nWho is qualified? : \n");
        for (Kandydat e : potential) {
            System.out.println(Qualified(e));
        }
    }
}
