package pl.edu.uwm.wmii.alanszuszkiewicz.IOExam2;
// some parts aren't needed because I copied them over to Kandydat
import java.util.ArrayList;

public class Rekrutacja{
    private static int exp;
    private ArrayList<Kandydat> k;

    public Rekrutacja(int exp, ArrayList<Kandydat> k){
        Rekrutacja.exp = exp;
        this.k = k;
    }

    public static void setDoswiadczenie(int dosw){
        exp = exp + dosw; // set to 2 to add 2 more years
    }
    public static int getdoswiadczenie(){
        return exp;
    }

    public static boolean Qualified(Kandydat k){
        if (Rekrutacja.getdoswiadczenie() >= k.getDoswiadczenie()){
            return true;
        }
        else{
            return false;
        }
    }
}
