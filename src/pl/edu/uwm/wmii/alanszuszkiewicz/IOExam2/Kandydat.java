package pl.edu.uwm.wmii.alanszuszkiewicz.IOExam2;

import java.util.ArrayList;
import java.util.HashMap;

public class Kandydat implements Cloneable, Comparable<Kandydat> {
    private String nazwa;
    private Integer wiek;
    private String wyksztalcenie;
    private Integer doswiadczenie;

    public Kandydat(String name, int age, String background, int experience  ){
        this.nazwa = name;
        this.wiek = age;
        this.wyksztalcenie = background;
        this.doswiadczenie = experience;

    }

    public String getNazwa(){
        return this.nazwa;
    }

    public int getWiek(){
        return this.wiek;
    }

    public String getWyksztalcenie(){
        return this.wyksztalcenie;
    }


    public int getDoswiadczenie(){
        return this.doswiadczenie;
    }

    public int compareTo(Kandydat comp) {
        int compare_wyksztalcenie= this.wyksztalcenie.compareTo(comp.wyksztalcenie);
        if(compare_wyksztalcenie==0){
            return this.wyksztalcenie.compareTo(comp.wyksztalcenie);
        }
        int compare_wiek= this.wiek.compareTo(comp.wiek);
        if(compare_wiek==0){
            return this.wiek.compareTo(comp.wiek);
        }
        int compare_doswiad = this.doswiadczenie.compareTo(comp.doswiadczenie);
        if(compare_doswiad==0){
            return this.doswiadczenie.compareTo(comp.doswiadczenie);
        }
        return compare_wyksztalcenie;
    }

    public static boolean Qualified(Kandydat k) {
        if(k.getDoswiadczenie() >= Rekrutacja.getdoswiadczenie()) {
            return true;
        }
        return false;
    }

    public static HashMap<Integer, String> Recruitmentshower(ArrayList<Kandydat> klist) {
        HashMap<Integer, String>  map = new HashMap<>();
        for(Kandydat e : klist)
            if(Qualified(e))
                map.put(e.getDoswiadczenie(), e.getNazwa());
        return map;
    }

}

