package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium07;

import java.time.LocalDate;

public class Student extends Person{

    public String program;
    public double avgGrade;

    public Student(String surname, String[] name, LocalDate birthdate, boolean gender, String program, double AG){
        super(surname, name, birthdate, gender);
        this.program = program;
        this.avgGrade = AG;
    }

    public double getAvgGrade(){
        return avgGrade;
    }

    public void setAvgGrade(double entered){
        this.avgGrade = entered;
    }

    @Override
    public String getEntry(){
        return "This student is studying " + this.program + " with a Grade Point Average of " + this.avgGrade;
    }
}
