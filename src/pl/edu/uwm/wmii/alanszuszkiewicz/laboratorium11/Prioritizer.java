package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium11;

import java.util.*;

public class Prioritizer {
    public Prioritizer(){
        this.list = new PriorityQueue<>();
        this.disp();
    }

    public void disp(){
        Scanner keyboard = new Scanner(System.in);
        String[] words;
        String line = keyboard.nextLine();
        Iterator<String> it;
        while (!line.equals("end")){
            words = line.split(" ");
            it = Arrays.stream(words).iterator();
            if(it.hasNext()){
                it.next();
                if(words[0].equals("add")){
                    System.out.println("Added entry.");
                    int prio = 0;
                    StringBuilder writer= new StringBuilder();
                    if(it.hasNext()){
                        prio=Integer.parseInt(it.next());
                    }
                    while (it.hasNext()){
                        writer.append(it.next());
                        writer.append(" ");
                    }
                    list.add(new Task(prio,writer.toString()));
                }
                else if(words[0].equals("next")){
                    list.remove();
                    System.out.println("Removed entry.");
                }
                else {
                    System.out.println("Unrecognized command.");
                }
            }
            line = keyboard.nextLine();

        }
    }

    public void writer(){
        PriorityQueue<Task> copier = new PriorityQueue<>(this.list);
        while(!copier.isEmpty()){
            Task e = copier.remove();
            System.out.println("Priority: " + e.priority);
            System.out.println("Description: " + e.desc);
        }
    }

    private PriorityQueue <Task> list;
}

class Task implements Comparable<Task>{
    public Task(int priority, String desc){
        this.desc = desc;
        this.priority = priority;
    }

    int priority;
    String desc;

    @Override
    public int compareTo(Task o) {
        return Integer.compare(this.priority, o.priority);
    }
}
