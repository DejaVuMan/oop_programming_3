package pl.edu.uwm.wmii.alanszuszkiewicz.laboratorium10;

import java.util.BitSet;
import java.util.LinkedList;
import java.util.Stack;

public class Zadania {

    public static void main(String[] args) {
        LinkedList<String> employees = new LinkedList<>();
        employees.add("Andrzej Kulowski");
        employees.add("Adrian Tang");
        employees.add("Mickey Mouse");
        employees.add("Joe Mama");
        System.out.println(employees);
        reduction(employees, 2);
        System.out.println();
        System.out.println(employees);
        reverse(employees);
        System.out.println();
        System.out.println(employees);
        String x = "Talk like people. Walk like crab. Craaaaab people.";
        System.out.println();
        System.out.println("Original String: " + x);
        System.out.println("Modified String: " + grogu(x));
        LinkedList<Integer> digits = new LinkedList<>();
        digits.add(1);
        digits.add(2);
        digits.add(3);
        System.out.println(digits);
        Zadania.digits(2015);
        System.out.println();
        Zadania.Erastotenes(100);
    }

    public static <T> void reduction(LinkedList<T> workers, int n) {
        for (int i = n - 1; i < workers.size(); i += n - 1) {
            workers.remove(i);
        }
    }

    public static <T> void reverse(LinkedList<T> ulist){
        LinkedList<T> holder = new LinkedList<>(ulist);
        for(int i =ulist.size()-1, j = 0; i >= 0; i--, j++){
            ulist.set(j, holder.get(i));
        }
    }

    public static String grogu(String sentence){ // Seriously, did anyone expect Baby Yoda's name to be G r o g u ?
        String[] words = sentence.split(" ");
        StringBuilder garbled = new StringBuilder();
        Stack<String> grogu = new Stack<>();
        for(String word : words){
            grogu.push(word);
            if(word.endsWith(".")){
                while((!grogu.empty())){
                    StringBuilder reversedphrase = new StringBuilder();
                    reversedphrase.append(grogu.pop());
                    if(grogu.empty()){
                        reversedphrase.setCharAt(0, Character.toLowerCase(reversedphrase.charAt(0)));
                        garbled.append(reversedphrase);
                        garbled.append(". ");
                    }
                    else if(reversedphrase.toString().equals(word)){
                        reversedphrase.setCharAt(0, Character.toUpperCase(reversedphrase.charAt(0)));
                        garbled.append(reversedphrase, 0, reversedphrase.length()-1);
                        garbled.append(" ");
                    }
                    else{
                        garbled.append(reversedphrase);
                        garbled.append(" ");
                    }
                }
            }
        }
        return garbled.toString();
    }

    public static void digits(int num){
        Stack<Integer> indiv = new Stack<>();
        while(num!=0){
            indiv.push(num%10);
            num/=10;
        }
        while (!indiv.empty()){
            System.out.print(indiv.pop()+" ");
        }
    }

    public static void Erastotenes(int n){
        BitSet b = new BitSet(n + 1);
        for (int j = 2; j <= n; ++j) {
            b.set(j);
        }
        int j = 2;
        while (j * j <= n) {
            if (b.get(j)) {
                int k = 2 * j;
                while (k <= n) {
                    b.clear(k);
                    k += j;
                }
            }
            ++j;
        }
        int[] prime= b.stream().toArray();
        for(int x: prime){
            System.out.print(x+" ");
        }
        System.out.println();
    }

}
