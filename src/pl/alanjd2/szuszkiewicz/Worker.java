package pl.alanjd2.szuszkiewicz;

import java.time.LocalDate;

public class Worker extends Person {

    private double salary;
    private LocalDate datehired;

    public Worker(String surname, String[] name, LocalDate birthdate, boolean gender, double salary, LocalDate DH){
        super(surname, name, birthdate, gender);
        this.salary = salary;
        this.datehired = DH;
    }

    public double getSalary(){
        return salary;
    }

    public LocalDate getDatehired(){
        return datehired;
    }

    @Override
    public String getEntry() {
        return String.format("This worker was hired on %s, with a salary of %.2f USD.",
                this.datehired.toString(), this.salary);
    }
}