package pl.alanjd2.szuszkiewicz.instruments;

import java.time.LocalDate;

public class Violin extends Instrument{
    public Violin(String manufacturer, LocalDate dOp){
        super(manufacturer, dOp);
    }

    public String sound(){
        return "*loud noises that make you regret having ears*";
    }
}