package pl.alanjd2.szuszkiewicz.instruments;

import java.time.LocalDate;

public class Flute extends Instrument{
    public Flute(String manufacturer, LocalDate dOp){
        super(manufacturer, dOp);
    }

    public String sound(){
        return "*loud 4th grade flute noises*";
    }
}