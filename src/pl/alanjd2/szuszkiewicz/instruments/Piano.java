package pl.alanjd2.szuszkiewicz.instruments;

import java.time.LocalDate;

public class Piano extends Instrument{
    public Piano(String manufacturer, LocalDate dOp){
        super(manufacturer, dOp);
    }

    public String sound(){
        return "*poorly played darude sandstorm in piano form*";
    }
}