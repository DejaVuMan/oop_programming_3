package pl.alanjd2.szuszkiewicz.instruments;

import java.time.LocalDate;


public class Driver {

    public static void main(String[] args) {
        Instrument[] specifics = new Instrument[1];

        specifics[0] = new Flute("Yamaha", LocalDate.of(2016, 1, 1));

        for (Instrument ins : specifics) {
            System.out.print(ins.sound() + "\n");
            System.out.println(ins);
        }
    }
}
