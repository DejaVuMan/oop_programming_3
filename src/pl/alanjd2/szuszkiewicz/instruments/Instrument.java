package pl.alanjd2.szuszkiewicz.instruments;

import java.time.LocalDate;

public abstract class Instrument {

    private String manufacturer;
    private LocalDate dateProduced;
    private Object obj;

    public Instrument(String manufacturer, LocalDate dOp){
        this.manufacturer = manufacturer;
        this.dateProduced = dOp;
    }

    public abstract String sound();

    public String getManufacturer() {
        return this.manufacturer;
    }

    public LocalDate getDateProduced(){
        return this.dateProduced;
    }

    @Override
    public boolean equals(Object obj) {
        return this.toString().equals(obj.toString());
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+" was created by " + this.manufacturer
                + " on "+this.dateProduced;
    }
}