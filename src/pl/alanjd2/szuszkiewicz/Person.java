package pl.alanjd2.szuszkiewicz;

import java.time.LocalDate;

public abstract class Person {

    private String surname;
    private String[] name;
    private LocalDate birthdate;
    private boolean gender;

    public Person(String surname, String[] name, LocalDate birthdate, boolean gender){
        this.surname = surname;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
    }

    public abstract String getEntry();

    public String getSurname(){
        return surname;
    }

    public String[] getName(){
        return name;
    }

    public LocalDate getBirthdate(){
        return birthdate;
    }

    public boolean getGender(){
        return gender;
    }
}