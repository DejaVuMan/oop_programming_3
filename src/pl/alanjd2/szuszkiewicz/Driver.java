package pl.alanjd2.szuszkiewicz;

import java.time.LocalDate;

public class Driver {

    public static void main(String[] args){

        Person[] people = new Person[1];

        people[0] = new Worker("Ramos", new String[]{"Daniel"}, LocalDate.of(1992, 2, 8),
                false, 72000, LocalDate.of(2013, 7, 22));

        for(Person p : people){
            for(String i: p.getName()){
                System.out.print(i + " ");
            }
            System.out.print(p.getSurname() + " - " + p.getEntry()
                    + " \nThey were born on " + p.getBirthdate() + " and are ");
            if(p.getGender()){
                System.out.println("female.");
            }
            else
            {
                System.out.println("male.");
            }
        }

    }
}