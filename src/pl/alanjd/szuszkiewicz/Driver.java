package pl.alanjd.szuszkiewicz;

public class Driver {

    public static void main(String[] args){

        Adres test1 = new Adres("Chicago Ave.", "4300",
                "15", "60601", "Chicago");
        Adres test2 = new Adres("Wacker Drive", "1200",
                "3", "60602", "Chicago");

        test1.show();
        test2.show();
        //////////////////////////
        Osoba Alan = new Osoba("Szuszkiewicz", 2001);
        Student Mickey = new Student("Mouse", 1943, "Film");
        Nauczyciel Robbie = new Nauczyciel("Romano", 1989, 78000);

        System.out.println(Alan);
        System.out.println(Mickey);
        System.out.println(Robbie);
        System.out.println(Robbie.getSalary());
        //////////////////////////
        BetterRectangle rectObj = new BetterRectangle(0,0,5,6);
        System.out.println(rectObj.getPerimeter());
        System.out.println(rectObj.getArea());

    }
}
